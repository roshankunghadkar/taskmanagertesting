*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/Task_Resources.robot
Library  DataDriver  ../TestData/TestCasesForRobo.xls   sheet_name=MembersTasksToEdit
Test Setup       Open My Browser    ${URL}
Test Teardown    Close Browsers
Test Template   Test Case to Edit Task

*** Variables ***

*** Test Cases ***
Member Updating Task

*** Keywords ***
Test Case to Edit Task
    [Arguments]     ${task_id}  ${username}   ${password}  ${updatedName}   ${updatedDesc}     ${updatedComment}   ${updatedPriority}
    Should match regexp  ${updatedName}  ^[A-Z].*$                 #First Letter should be in Capital Case
    should match regexp  ${updatedDesc}   ^.{0,200}$               #maximum Description length 200
    should match regexp  ${updatedComment}   ^.{0,100}$            #Maximum Comment Length 100
    Should match regexp  ${updatedPriority}  ^[Pp][1-5]            #Priority must be P1-P5
    should match regexp  ${username}   ^.{4,12}$                   #username must between 4-12 characters
    should match regexp  ${password}   ^.{4,15}$                   #password must between 4-15 characters
    should match regexp  ${password}   ^[ A-Za-z0-9_@.!#&-]*$      #password contains only specific characters

    Open Login Page
    Enter Username  ${username}
    Enter Password  ${password}
    Click Login Button
    Available Tasks should be visible
    go to         ${URL}/update/${task_id}/${username}
    Enter TaskName      ${updatedName}
    Enter TaskDescription       ${updatedDesc}
    Enter TaskComment  ${updatedComment}
    Enter Priority  ${updatedPriority}
    Click Button  xpath://button[@id='submitUpdate']
    page should contain     ${updatedName}
