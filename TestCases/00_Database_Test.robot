*** Settings ***
Library           MongoDBLibrary
Resource  ../Resources/Task_Resources.robot

*** Variables ***
${MDBHost}        ${DBURL}
${MDBPort}        27017
${DataBase}        mydb1
${Collection}        admin

*** Test Cases ***
Connect-Disconnect
    Comment    Connect to MongoDB Server
    Connect to MongoDB    ${MDBHost}    ${MDBPort}
    Comment    Disconnect from MongoDB Server
    Disconnect From MongoDB

Get MongoDB Databases
    Connect To MongoDB    ${MDBHost}    ${MDBPort}
    Comment    Retrieve a list of databases on the MongoDB server
    ${output} =    Get MongoDB Databases
    Comment    Verify that db name is contained in the list output
    Should Contain    ${output}    ${DataBase}
    log many  ${output}
    Disconnect From MongoDB


Get MongoDB Collections
    Connect To MongoDB    ${MDBHost}    ${MDBPort}
    ${output}    Get MongoDB Collections      ${DataBase}
    Comment    Log as an array the returned list of collections
    Log Many    ${output}
    Comment    Verify that known collection name is in the list returned
    Comment    Verify that known collection name is in the list returned
    Should Contain    ${output}    ${Collection}
    Disconnect From MongoDB

#Validate MongoDB Collection
#    ${MDBDB} =    Set Variable    foo
#    Connect To MongoDB    ${MDBHost}    ${MDBPort}
#    ${MDBColl} =    Set Variable    foo
#    Comment    Validate a Collection
#    ${allResults} =    Validate MongoDB Collection    ${MDBDB}    ${MDBColl}
#    Log    ${allResults}
#    ${MDBColl} =    Set Variable    system.indexes
#    Comment    Validate a Collection
#    ${allResults} =    Validate MongoDB Collection    ${MDBDB}    ${MDBColl}
#    Log    ${allResults}
#    Disconnect From MongoDB
