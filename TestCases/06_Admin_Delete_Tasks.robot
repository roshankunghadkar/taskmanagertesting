*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/Task_Resources.robot
Library  DataDriver  ../TestData/TestCasesForRobo.xls   sheet_name=AdminTasksToDelete
Test Setup       Admin Login    ${AdminName}    ${AdminPass}
Test Teardown    Close Browsers
Test Template    Admin Delete Task

*** Variables ***
${AdminName}=  ${ADMIN CREDS.AdminName}         #Taking creds from external yaml file
${AdminPass}=  ${ADMIN CREDS.AdminPass}

*** Test Cases ***
Admin Deleting Task


*** Keywords ***
Admin Delete Task
    [Arguments]      ${task_id}
    Click Add Task
    Available Tasks should be visible
    go to       ${URL}/deleteAdmin/${task_id}/${AdminName}
    Available Tasks should be visible

