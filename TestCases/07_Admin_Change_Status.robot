*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/Task_Resources.robot
Library  DataDriver  ../TestData/TestCasesForRobo.xls   sheet_name=AdminTasksToChangeStatus
Test Setup       Admin Login    ${AdminName}    ${AdminPass}
Test Teardown    Close Browsers
Test Template    Admin Changing Status

*** Variables ***
${AdminName}=  ${ADMIN CREDS.AdminName}         #Taking creds from external yaml file
${AdminPass}=  ${ADMIN CREDS.AdminPass}

*** Test Cases ***
Admin Changing Status of Task

*** Keywords ***
Admin Changing Status
    [Arguments]   ${task_id}
    Click Add Task
    Available Tasks should be visible
    go to         ${URL}/doneAdmin/${task_id}/${AdminName}
    page should contain     Complete


