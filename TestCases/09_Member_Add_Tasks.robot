*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/Task_Resources.robot
Library  DataDriver  ../TestData/TestCasesForRobo.xls   sheet_name=MembersTasksToAdd
Suite Setup       Open My Browser   ${URL}
Suite Teardown    Close Browsers
Test Template     Member Add Tasks

*** Variables ***

*** Test Cases ***
Member Adding Task With Exel

*** Keywords ***
Member Add Tasks
        [Arguments]        ${username}  ${password}   ${TaskName}  ${TaskDescription}  ${TaskComment}  ${Priority}
        Should match regexp  ${TaskName}  ^[A-Z].*$                 #First Letter should be in Capital Case
        should match regexp  ${TaskDescription}   ^.{0,200}$        #maximum Description length 200
        should match regexp  ${TaskComment}   ^.{0,100}$            #Maximum Comment Length 100
        Should match regexp  ${Priority}  ^[Pp][1-5]                #Priority must be P1-P5
        should match regexp  ${username}   ^.{4,12}$                   #username must between 4-12 characters
        should match regexp  ${password}   ^.{4,15}$                   #password must between 4-15 characters
        should match regexp  ${password}   ^[ A-Za-z0-9_@.!#&-]*$      #password contains only specific characters

        Open Login Page
        Enter Username      ${username}
        Enter Password      ${password}
        Click Login Button
        user Should be visible   ${username}

        Enter TaskName      ${TaskName}
        Enter TaskDescription      ${TaskDescription}
        Enter TaskComment      ${TaskComment}
        Enter Priority        ${Priority}
        Click Create Button
        Added Task should be visible   ${TaskName}
