*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/Task_Resources.robot
Library  DataDriver  ../TestData/TestCasesForRobo.xls   sheet_name=AdminTasksToAssign
Test Setup       Admin Login    ${AdminName}    ${AdminPass}
Test Teardown    Close Browsers
Test Template    Assign Task

*** Variables ***
${AdminName}=  ${ADMIN CREDS.AdminName}         #Taking creds from external yaml file
${AdminPass}=  ${ADMIN CREDS.AdminPass}

*** Test Cases ***
Admin Assigning Task to Member

*** Keywords ***
Assign Task
    [Arguments]   ${Task_Id}    ${username}
    Click Add Task
    Available Tasks should be visible
    go to         ${URL}/assign/${Task_Id}
    go to         ${URL}/assign/${Task_Id}/${username}
    Admin Dashboard should be visible
