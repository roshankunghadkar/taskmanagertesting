*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/Task_Resources.robot
Library  DataDriver  ../TestData/TestCasesForRobo.xls   sheet_name=AdminTasksToEdit
Test Setup       Admin Login    ${AdminName}    ${AdminPass}
Test Teardown    Close Browsers
Test Template    Admin Edit Task

*** Variables ***
${AdminName}=  ${ADMIN CREDS.AdminName}         #Taking creds from external yaml file
${AdminPass}=  ${ADMIN CREDS.AdminPass}

*** Test Cases ***
Admin Updating Task

*** Keywords ***
Admin Edit Task
    [Arguments]     ${task_id}  ${updatedName}   ${updatedDesc}     ${updatedComment}   ${updatedPriority}
    Should match regexp  ${updatedName}  ^[A-Z].*$                 #First Letter should be in Capital Case
    should match regexp  ${updatedDesc}   ^.{0,200}$               #maximum Description length 200
    should match regexp  ${updatedComment}   ^.{0,100}$            #Maximum Comment Length 100
    Should match regexp  ${updatedPriority}  ^[Pp][1-5]            #Priority must be P1-P5

    Click Add Task
    Available Tasks should be visible
    go to         ${URL}/updateAdmin/${task_id}/${AdminName}
    Enter TaskName      ${updatedName}
    Enter TaskDescription       ${updatedDesc}
    Enter TaskComment  ${updatedComment}
    Enter Priority  ${updatedPriority}
    Click Button  xpath://button[@id='submitUpdate']
    page should contain     ${updatedName}
