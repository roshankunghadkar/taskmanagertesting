*** Settings ***
Library  SeleniumLibrary
Library  OperatingSystem
Library  String
Resource  ../Resources/Task_Resources.robot
Library  DataDriver  ../TestData/TestCasesForRobo.xls   sheet_name=MembersToAdd

Suite Setup       Admin Login   ${AdminName}   ${AdminPass}
Suite Teardown    Close Browsers
Test Template     Admin Add Members

*** Variables ***
${AdminName}=  ${ADMIN CREDS.AdminName}         #Taking creds from external yaml file
${AdminPass}=  ${ADMIN CREDS.AdminPass}

*** Test Cases ***
Admin Adding Members

*** Keywords ***
Admin Add Members
        [Arguments]     ${username}     ${password}
        should match regexp  ${username}   ^.{4,12}$                   #username must between 4-12 characters
        should match regexp  ${password}   ^.{4,15}$                   #password must between 4-15 characters
        should match regexp  ${password}   ^[ A-Za-z0-9_@.!#&-]*$      #password contains only specific characters
        Admin Dashboard should be visible
        Click Add Member
        Enter Username      ${username}
        Enter Password      ${password}
        Click Add Member
        Admin Dashboard should be visible



