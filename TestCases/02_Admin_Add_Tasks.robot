*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/Task_Resources.robot
Library  DataDriver  ../TestData/TestCasesForRobo.xls   sheet_name=AdminTasksToAdd
Suite Setup       Admin Login   ${AdminName}    ${AdminPass}
Suite Teardown    Close Browsers
Test Template     Admin Add Task

*** Variables ***
${AdminName}=  ${ADMIN CREDS.AdminName}         #Taking creds from external yaml file
${AdminPass}=  ${ADMIN CREDS.AdminPass}

*** Test Cases ***
Admin Adding Task With Exel

*** Keywords ***
Admin Add Task
        [Arguments]     ${TaskName}  ${TaskDescription}  ${TaskComment}  ${Priority}
        Should match regexp  ${TaskName}  ^[A-Z].*$                 #First Letter should be in Capital Case
        should match regexp  ${TaskDescription}   ^.{0,200}$        #maximum Description length 200
        should match regexp  ${TaskComment}   ^.{0,100}$            #Maximum Comment Length 100
        Should match regexp  ${Priority}  ^[Pp][1-5]                #Priority must be P1-P5
        Click Add Task
        Available Tasks should be visible
        Enter TaskName      ${TaskName}
        Enter TaskDescription      ${TaskDescription}
        Enter TaskComment      ${TaskComment}
        Enter Priority        ${Priority}
        Click Create Button
        Added Task should be visible   ${TaskName}
