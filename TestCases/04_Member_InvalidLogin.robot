*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/Task_Resources.robot
Library  DataDriver  ../TestData/TestCasesForRobo.xls   sheet_name=MembersInvalidLogin
Suite Setup       Open My Browser   ${URL}
Suite Teardown    Close Browsers
Test Template   InvalidLogin user

*** Variables ***

*** Test Cases ***
Member Invalid Login

*** Keywords ***
InvalidLogin user
        [Arguments]     ${username}     ${password}
        Open Login Page
        Enter Username      ${username}
        Enter Password      ${password}
        Click Login Button
        InvalidLogin should be visible
