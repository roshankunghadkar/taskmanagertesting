*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/Task_Resources.robot
Library  DataDriver  ../TestData/TestCasesForRobo.xls   sheet_name=MembersTasksToDelete
Test Setup       Open My Browser    ${URL}
Test Teardown    Close Browsers
Test Template    Test Case to Delete Task

*** Variables ***

*** Test Cases ***
Member Deleting Tasks

*** Keywords ***
Test Case to Delete Task
    [Arguments]     ${task_id}  ${username}   ${password}
    should match regexp  ${username}   ^.{4,12}$                   #username must between 4-12 characters
    should match regexp  ${password}   ^.{4,15}$                   #password must between 4-15 characters
    should match regexp  ${password}   ^[ A-Za-z0-9_@.!#&-]*$      #password contains only specific characters

    Open Login Page
    Enter Username  ${username}
    Enter Password  ${password}
    Click Login Button
    Available Tasks should be visible
    go to       ${URL}/delete/${task_id}/${username}
    Available Tasks should be visible
