pipeline{
    agent any
    environment {
       email_list = 'irfan.hussain@gslab.com'
   }
    tools {
        terraform 'Terraform'
    }

    stages {
        stage('scanning for vulnerabilities'){
            steps{
                sh 'sh scan.sh'

            }
        }
        stage('git checkout taskmanager'){
            steps{
                git credentialsId: 'jenkins-gitlab', url: 'https://gitlab.com/roshankunghadkar/taskmanager.git'
            }
        }
        
        stage('Code Quality Check via SonarQube'){
            steps {
               script {
               def scannerHome = tool 'sonarqube-scanner';
                   withSonarQubeEnv("sonarqube-scanner") {
                   sh 'cd  /var/lib/jenkins/workspace/TestAutomation-PoC'
                   sh "${tool("sonarqube-scanner")}/bin/sonar-scanner"
                       }
                   }
                }
        }

        stage ("SonarQube Gatekeeper") {
            steps {
                script {
                    STAGE_NAME = "SonarQube Gatekeeper"
                            def qualitygate = waitForQualityGate()
                
                            if (qualitygate.status != "OK") {
                                sh 'curl --request POST --header "PRIVATE-TOKEN: sEqyNJVR4fx7okjRW5S5" "https://gitlab.com/api/v4/projects/24895034/issues?title=Issues%20with%20sonarqube&labels=bug"'  
                               error "Pipeline aborted due to quality gate coverage failure: ${qualitygate.status}"    
               
                            }
                    }
            }
        }
        stage('git checkout taskmanagertesting'){
            steps{
                git credentialsId: 'jenkins-gitlab', url: 'https://gitlab.com/roshankunghadkar/taskmanagertesting.git'
            }
        }
        stage('Azure Login') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'azure-creds', passwordVariable: 'PW', usernameVariable: 'ID')]) {
                            sh 'az login -u $ID -p $PW'
                        }
                
            }
                   
        }
        stage('terraform init'){
            steps{
                sh 'terraform init'
            }
        }
        stage('terraform apply'){
            steps{
                sh 'terraform apply -var="resourcegroup=${ResourceGroup}" -var="location=${Location}" -var="vmname=${VMname}" --auto-approve'
            }
        }
        stage('fetching ip from tf-backup file'){
            steps{
                sh 'sh ip_fetch.sh'
            }
        }
        stage('running test cases'){
            steps{
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE'){
                    sleep time: 10000, unit: 'MILLISECONDS'
                    sh  'bash rerun.sh'
                    sh '''python3 -m rflint --ignore LineTooLong /var/lib/jenkins/workspace/TestAutomation-PoC'''
                    sh '''python3 -m robot --outputdir /var/lib/jenkins/workspace/Robot-reports -V Resources/creds.yaml TestCases/'''
                    //sh '''python3 -m robot --outputdir /var/lib/jenkins/workspace/Robot-reports -V Resources/creds.yaml TestCases/03_Member_Login.robot'''
                    
                    sh 'exit 0'

                }
                
            }
            post {
                success {
                    sh 'cp /var/lib/jenkins/workspace/Robot-reports/report.html /var/lib/jenkins/workspace/TestAutomation-PoC/'
                    notifySuccessful()
                    
                }
                failure {
                    sh 'cp /var/lib/jenkins/workspace/Robot-reports/report.html /var/lib/jenkins/workspace/TestAutomation-PoC/' 
                    notifyFailed()
                }
            }
        }
        stage('display test reports'){
            steps{
                publishHTML (target : [allowMissing: false,
                         alwaysLinkToLastBuild: true,
                         keepAll: true,
                         reportDir: '/var/lib/jenkins/workspace/Robot-reports',
                         reportFiles: 'log.html',
                         reportName: 'Test Reports',
                         reportTitles: 'The Report'])
            }
        }
    }
    post{
        failure{   
            
            build job: 'issue-creation'
            echo 'Some Test Cases Failed. Testing Infrastructure will be Preserve for further Testing'
            error 'Test Case failed'
        }
        success {
            
            echo 'All Test cases passed. Testing infrastructure will be Destroyed'
            sh 'terraform destroy --auto-approve'
            cleanWs()
            
        }
    
    }
}

def notifySuccessful() {
  emailext (
      subject: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
      attachmentsPattern: 'report.html',
      body: """<p>SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
        <p>Check console output at <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a></p>""",
      recipientProviders: [[$class: 'DevelopersRecipientProvider']],
      to: "${email_list}"
    )
}

def notifyFailed() {
  emailext (
      subject: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
      attachmentsPattern: 'report.html',
      body: """<p>FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
        <p>Check console output at <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a></p>""",
      recipientProviders: [[$class: 'DevelopersRecipientProvider']],
      to: "${email_list}"
    )
}


