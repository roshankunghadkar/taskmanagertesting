provider "azurerm" {
   features {}
}

#Azure Resource-Group creation
resource "azurerm_resource_group" "myterraformgroup" {
  name     = "${var.resourcegroup}"
  location = "${var.location}"
}

#Azure virtual_network creation
resource "azurerm_virtual_network" "mynetwork" {
  name                = "${var.vname}"
  address_space       = "${var.addressspace}"
  location            = "${azurerm_resource_group.myterraformgroup.location}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
}

#Azure subnet creation
resource "azurerm_subnet" "subnet" {
  name                 = "${var.subname}"
  resource_group_name  = "${azurerm_resource_group.myterraformgroup.name}"
  virtual_network_name = "${azurerm_virtual_network.mynetwork.name}"
  address_prefix     = "${var.addressprefix}"
}

#Azure network_security_group creation
resource "azurerm_network_security_group" "web_server_nsg"{
  name                 = "terraformdemo"
  location             = "${azurerm_resource_group.myterraformgroup.location}"
  resource_group_name  = "${azurerm_resource_group.myterraformgroup.name}"
}

#Configuring NSG with subnet
resource "azurerm_subnet_network_security_group_association" "configuration" {
  subnet_id  = azurerm_subnet.subnet.id
  network_security_group_id = azurerm_network_security_group.web_server_nsg.id
}

#Creating Inbound rule
resource "azurerm_network_security_rule" "web_server_nsg_rule_db" {
  name                         = "AllowHttpTraffic"
  priority                     = 101
  direction                    = "Inbound"
  access                       = "Allow"
  protocol                     = "*"
  source_port_range            = "*"
  destination_port_ranges      = ["40", "443", "27017","22","5000"]
  source_address_prefix        = "*"
  destination_address_prefix   = "*"
  resource_group_name          = "${azurerm_resource_group.myterraformgroup.name}"
  network_security_group_name  = "${azurerm_network_security_group.web_server_nsg.name}"
}

#Creating Outbound rule
resource "azurerm_network_security_rule" "web_server_nsg_rule_db1" {
  name                         = "DB Outbound"
  priority                     = 102
  direction                    = "Outbound"
  access                       = "Allow"
  protocol                     = "*"
  source_port_range            = "*"
  destination_port_ranges       = ["40", "443", "27017","22","5000"]
  source_address_prefix        = "*"
  destination_address_prefix   = "*"
  resource_group_name          = "${azurerm_resource_group.myterraformgroup.name}"
  network_security_group_name  = "${azurerm_network_security_group.web_server_nsg.name}"
}

#Azure public_ip creation
resource "azurerm_public_ip" "name"{
  name                 = "${var.publicip}"
  location             = "${azurerm_resource_group.myterraformgroup.location}"
  resource_group_name  = "${azurerm_resource_group.myterraformgroup.name}"
  allocation_method    = "Static"
}

resource "azurerm_network_interface" "demonic" {
  name                = "${var.machinename}"
  location            = "${azurerm_resource_group.myterraformgroup.location}"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"

  ip_configuration {
    name                          = "testconfiguration1"
    subnet_id                     = "${azurerm_subnet.subnet.id}"
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = "${azurerm_public_ip.name.id}"
  }
}

#Azure VM creation
resource "azurerm_virtual_machine" "azurevm" {
  name                  = "${var.vmname}"
  location              = "${azurerm_resource_group.myterraformgroup.location}"
  resource_group_name   = "${azurerm_resource_group.myterraformgroup.name}"
  network_interface_ids = ["${azurerm_network_interface.demonic.id}"]
  vm_size               = "${var.vmsize}"



  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "myosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "hostname"
    admin_username = "gslab"
    admin_password = "GSLab123"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }



  
}
resource "null_resource" "example" {

    connection {
        type = "ssh"
        user = "gslab"
        password = "GSLab123"
        host = azurerm_public_ip.name.ip_address
        port = 22
    }
    provisioner "file" {
        source = "/var/lib/jenkins/workspace/TestAutomation-PoC/script.sh"
        destination = "/tmp/script.sh"
    }
    

    provisioner "remote-exec" {
        inline = [
            "/bin/bash /tmp/script.sh"
            
        ]
    }

    
}

  

output name {
  value       = "${azurerm_resource_group.myterraformgroup.name}"
}

output time {
  value       = "${timestamp()}"
}

output vmIP {
  value       = "${azurerm_public_ip.name.ip_address}"
}

resource "local_file" "vmIP" {
    content  = "$${ip}  ${azurerm_public_ip.name.ip_address}"
    filename = "Ip.txt"
}
resource "local_file" "Task-managerip" {
    content  = "${azurerm_public_ip.name.ip_address}"
    filename = "taskmanager-ip.txt"
}
