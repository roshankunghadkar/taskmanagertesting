*** Settings ***
Library  SeleniumLibrary
Library  OperatingSystem
Library  String

*** Keywords ***

#Open My Browser
#    open browser  ${URL}    ${Browser}
#    maximize browser window


Open My Browser
    [Arguments]    ${URL}
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys
    Call Method    ${chrome_options}    add_argument    --disable-dev-shm-usage
    Call Method    ${chrome_options}    add_argument     --no-sandbox
    Call Method    ${chrome_options}    add_argument     --headless
    Run Keyword If    os.sep == '/'    Create Webdriver    Chrome    my_alias    chrome_options=${chrome_options}    executable_path=/usr/bin/chromedriver
    ...    ELSE    Create Webdriver    Chrome    my_alias    chrome_options=${chrome_options}
    Set Window Size    1200    1000
    Go To    ${URL}

Open Browser to Page
    [Documentation]     Opens one of:
    ...                 - Google Chrome
    ...                 - Mozilla Firefox
    ...                 - Microsoft Internet Explorer
    ...                 to a given web page.
    [Arguments]    ${URL}
    Run Keyword If      '${BROWSER}' == 'Chrome'      Open Chrome Browser to Page                 ${URL}
    ...     ELSE IF     '${BROWSER}' == 'Firefox'     Open Firefox Browser to Page                ${URL}
    ...     ELSE IF     '${BROWSER}' == 'IE'          Open Internet Explorer to Page      ${URL}
    Set Selenium Speed              ${DELAY}

Close Browsers
        close all browsers

Admin Login
        [Arguments]     ${AdminName}       ${AdminPass}
        Open My Browser     ${URL}
        Enter Username      ${AdminName}
        Enter Password      ${AdminPass}
        Click Login Button

Open Login Page
        go to   ${URL}

Open Register Page
        go to   ${URL}/register

Enter Username
        [Arguments]     ${username}
        input text  name:Uname  ${username}

Enter Password
        [Arguments]     ${password}
        input text  name:Pass  ${password}

Click Register Button
        click element  id:register

Click Login Button
        click element  id:login


Click Delete Button
        click element  id:deleteTask

Click Add Member
        click element  id:addmember

Click Add Task
        click element  id:addtask

Click Edit Button
        click element  id:editTask


Enter TaskName
        [Arguments]  ${TaskName}
        input text  name:name   ${TaskName}

Enter TaskDescription
        [Arguments]  ${TaskDescription}
        input text  name:desc   ${TaskDescription}

Enter TaskComment
        [Arguments]  ${TaskComment}
        input text  name:comments  ${TaskComment}

Enter Priority
        [Arguments]  ${Priority}
        input text  name:priority  ${Priority}

Click Create Button
        click element  id:submitCreate

Click Done Task
        click element  id:doneTask

Click Undo Task
        click element  id:undoTask

Error message should be visible
        page should contain     There was an issue while adding your task

Tasks should be visible
        page should contain     Task Manager

Available Tasks should be visible
        page should contain  Available Tasks

Added Task should be visible
        [Arguments]    ${TaskName}
        page should contain   ${TaskName}

Login should be visible
        page should contain  Login Form

Register should be visible
        page should contain     Register Form

InvalidLogin should be visible
        page should contain  Invalid Credentials

user Should be visible
        [Arguments]    ${username}
        page should contain  ${username}

Deleted Tasks should not be visible
        page should not contain  Azure

Admin Dashboard should be visible
        page should contain  ADMIN DASHBOARD


*** Variables ***
${Browser}  Chrome
${URL}  http://${ip}:5000/
${DBURL}  ${ip}
${DELAY}    2
